<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files ("Software"), to deal
 * in Software without restriction, including without limitation rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of Software, and to permit persons to whom Software is
 * furnished to do so, subject to following conditions:
 *
 * above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of Software.
 *
 * SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH SOFTWARE OR USE OR OTHER DEALINGS IN
 * SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus Terisi.</div></div>';
$lang['form_validation_isset']			= '<div class="form-error-text-block"><div id="infoMessage">{field} field memiliki Nilai.</div></div>';
$lang['form_validation_valid_email']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus sesuai penulisan Email.</div></div>';
$lang['form_validation_valid_emails']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus sesuai dengan semua penulisan Email.</div></div>';
$lang['form_validation_valid_url']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus dengan format URL.</div></div>';
$lang['form_validation_valid_ip']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus dengan format IP.</div></div>';
$lang['form_validation_min_length']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field panjang karakter harus minimal {param} Karakter.</div></div>';
$lang['form_validation_max_length']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field panjang karakter harus maksimal {param} Karakter.</div></div>';
$lang['form_validation_exact_length']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus sebanyak {param} Karakter.</div></div>';
$lang['form_validation_alpha']			= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya boleh mengandung Alphabet.</div></div>';
$lang['form_validation_alpha_numeric']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya boleh mengandung Alphabet dan Angka.</div></div>';
$lang['form_validation_alpha_numeric_spaces']	= '<div class="form-error-text-block"><div id="infoMessage">{field} hanya boleh mengandung Alphabet , Angka dan Spasi.</div></div>';
$lang['form_validation_alpha_dash']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya boleh mengandung Alphabet , Angka, Garis bawah, dan Strip.</div></div>';
$lang['form_validation_numeric']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya boleh mengandung Angka.</div></div>';
$lang['form_validation_is_numeric']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya boleh mengandung Angka.</div></div>';
$lang['form_validation_integer']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya boleh mengandung Integer.</div></div>';
$lang['form_validation_regex_match']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field tidak sesuai Format.</div></div>';
$lang['form_validation_matches']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field tidak sama dengan {param} field.</div></div>';
$lang['form_validation_differs']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus berbeda dari {param} field.</div></div>';
$lang['form_validation_is_unique'] 		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus mengandung nilai Unik.</div></div>';
$lang['form_validation_is_natural']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya harus berisi Angka.</div></div>';
$lang['form_validation_is_natural_no_zero']	= '<div class="form-error-text-block"><div id="infoMessage">{field} field hanya harus berisi angka dan harus lebih besar dari Nol.</div></div>';
$lang['form_validation_decimal']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus mengandung Desimal.</div></div>';
$lang['form_validation_less_than']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus berisi angka kurang dari {param}.</div></div>';
$lang['form_validation_less_than_equal_to']	= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus berisi angka kurang dari atau sama dengan {param}.</div></div>';
$lang['form_validation_greater_than']		= '<div class="form-error-text-block"><div id="infoMessage">{field} field harus berisi angka lebih besar dari {param}.</div></div>';
$lang['form_validation_greater_than_equal_to']	= '<div class="form-error-text-block"><div id="infoMessage">{field} harus berisi angka yang lebih besar dari atau sama dengan {param}.</div></div>';
$lang['form_validation_error_message_not_set']	= ' Tidak dapat mengakses pesan kesalahan yang sesuai dengan nama field Anda {field}.</div></div>';
$lang['form_validation_in_list']		= '<div class="form-error-text-block"><div id="infoMessage">{field} harus menjadi salah satu: {param}.</div></div>';
