<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field is required.</div></div>';
$lang['form_validation_isset']			= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must have a value.</div></div>';
$lang['form_validation_valid_email']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a valid email address.</div></div>';
$lang['form_validation_valid_emails']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain all valid email addresses.</div></div>';
$lang['form_validation_valid_url']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a valid URL.</div></div>';
$lang['form_validation_valid_ip']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a valid IP.</div></div>';
$lang['form_validation_min_length']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must be at least {param} characters in length.</div></div>';
$lang['form_validation_max_length']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field cannot exceed {param} characters in length.</div></div>';
$lang['form_validation_exact_length']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must be exactly {param} characters in length.</div></div>';
$lang['form_validation_alpha']			= '<div class="form-error-text-block"><div id="infoMessage">The {field} field may only contain alphabetical characters.</div></div>';
$lang['form_validation_alpha_numeric']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field may only contain alpha-numeric characters.</div></div>';
$lang['form_validation_alpha_numeric_spaces']	= '<div class="form-error-text-block"><div id="infoMessage">The {field} field may only contain alpha-numeric characters and spaces.</div></div>';
$lang['form_validation_alpha_dash']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field may only contain alpha-numeric characters, underscores, and dashes.</div></div>';
$lang['form_validation_numeric']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain only numbers.</div></div>';
$lang['form_validation_is_numeric']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain only numeric characters.</div></div>';
$lang['form_validation_integer']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain an integer.</div></div>';
$lang['form_validation_regex_match']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field is not in the correct format.</div></div>';
$lang['form_validation_matches']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field does not match the {param} field.</div></div>';
$lang['form_validation_differs']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must differ from the {param} field.</div></div>';
$lang['form_validation_is_unique'] 		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a unique value.</div></div>';
$lang['form_validation_is_natural']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must only contain digits.</div></div>';
$lang['form_validation_is_natural_no_zero']	= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must only contain digits and must be greater than zero.</div></div>';
$lang['form_validation_decimal']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a decimal number.</div></div>';
$lang['form_validation_less_than']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a number less than {param}.</div></div>';
$lang['form_validation_less_than_equal_to']	= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a number less than or equal to {param}.</div></div>';
$lang['form_validation_greater_than']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a number greater than {param}.</div></div>';
$lang['form_validation_greater_than_equal_to']	= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must contain a number greater than or equal to {param}.</div></div>';
$lang['form_validation_error_message_not_set']	= 'Unable to access an error message corresponding to your field name {field}.</div></div>';
$lang['form_validation_in_list']		= '<div class="form-error-text-block"><div id="infoMessage">The {field} field must be one of: {param}.</div></div>';
