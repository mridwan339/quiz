<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		//$this->load->library(array('ion_auth','form_validation'));
		
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		
	}
	public static function header()
	{
	
		echo '
		
		<!DOCTYPE html>
		<html>
		<head lang="en">
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title>FarahQuiz</title>

			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="icon" type="image/png">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/favicon.ico" rel="shortcut icon">

			<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/separate/vendor/slick.min.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/separate/vendor/select2.min.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/separate/pages/widgets.min.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/lib/font-awesome/font-awesome.min.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/lib/bootstrap/bootstrap.min.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/main.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/separate/vendor/tags_editor.min.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/separate/vendor/bootstrap-select/bootstrap-select.min.css">
			<link rel="stylesheet" href="'; echo base_url(); echo 'templates/StartUI/build/css/separate/vendor/select2.min.css">
			<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.css">
		
			<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.css">
			<style>
				.sort {
				  padding:0px 30px;
				  border-radius: 6px;
				  border:none;
				  display:inline-block;
				  color:#fff;
				  text-decoration: none;
				  background-color: #28a8e0;
				  height:30px;
				  margin-bottom:10px;
				}
				.sort:hover {
				  text-decoration: none;
				  background-color:#1b8aba;
				}
				.sort:focus {
				  outline:none;
				}
				.sort:after {
				  display:inline-block;
				  width: 0;
				  height: 0;
				  border-left: 5px solid transparent;
				  border-right: 5px solid transparent;
				  border-bottom: 5px solid transparent;
				  content:"";
				  position: relative;
				  top:-10px;
				  right:-5px;
				}
				.sort.asc:after {
				  width: 0;
				  height: 0;
				  border-left: 5px solid transparent;
				  border-right: 5px solid transparent;
				  border-top: 5px solid #fff;
				  content:"";
				  position: relative;
				  top:4px;
				  right:-5px;
				}
				.sort.desc:after {
				  width: 0;
				  height: 0;
				  border-left: 5px solid transparent;
				  border-right: 5px solid transparent;
				  border-bottom: 5px solid #fff;
				  content:"";
				  position: relative;
				  top:-4px;
				  right:-5px;
				}
				.pagination li {
				  display:inline-block;
				  padding:5px;
				}
				.select2-results{
					padding:0px 10px !important;
				}
				.select2-results li:hover{
					color: #86cc94 !important;
				}
				.select2-results__option[aria-selected=true]{
					color:#31d251 !important;
				}
				.select2-container--default .select2-results__option--highlighted[aria-selected]{
					color:#000;
				}
				.select2-container .select2-selection--single .select2-selection__rendered{
					padding-left: 17px;
					padding-right: 17px;
				}
			</style>
			
			<style type="text/css">
			#preview {
				background: red;
				border: 1px solid green;
			}
			
			</style>
			<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/jquery/jquery.min.js"></script>
			<script>
			var creatorID="";
			var q_type_alpha="";
			var q_kind_feed="";
			var q_question_id="";
			var select_answer="";
			</script>
			
		</head>
		
		';
	}
	public static function sidebar()
	{
		$ci =& get_instance();
		$current_user = $ci->ion_auth->user()->row();
		echo '<body class="with-side-menu">
		<header class="site-header">
			<div class="container-fluid">
		
				<a href="'.site_url('auth').'" class="site-logo">
					<img class="hidden-md-down" src="'; echo base_url(); echo 'templates/StartUI/build/img/logo-2.png" alt="">
					<img class="hidden-lg-up" src="'; echo base_url(); echo 'templates/StartUI/build/img/logo-2-mob.png" alt="">
				</a>
		
				<button id="show-hide-sidebar-toggle" class="show-hide-sidebar">
					<span>toggle menu</span>
				</button>
		
				<button class="hamburger hamburger--htla">
					<span>toggle menu</span>
				</button>
				<div class="site-header-content">
					<div class="site-header-content-in">
						<div class="site-header-shown">
							';/*
							<div class="dropdown dropdown-lang">
								<button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="flag-icon flag-icon-us"></span>
								</button>
								<div class="dropdown-menu dropdown-menu-right">
									<div class="dropdown-menu-col">
										<a class="dropdown-item" href="#"><span class="flag-icon flag-icon-id"></span>Indonesian</a>
									</div>
									<div class="dropdown-menu-col">
										<a class="dropdown-item current" href="#"><span class="flag-icon flag-icon-us"></span>English</a>
									</div>
								</div>
							</div>*/
							echo'
							<div class="dropdown user-menu">
								<button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="'.$current_user->avatar.'" alt="">
								</button>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
									<center><b>'.$current_user->email.'</b></center>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="'.base_url().'index.php/auth/edit_user/'.$current_user->id.'"><span class="font-icon glyphicon glyphicon-user"></span>Profile</a>
									<a class="dropdown-item" href="'.base_url().'index.php/auth/change_password/'.$current_user->id.'"><span class="font-icon glyphicon glyphicon-lock"></span>Ubah Password</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="'.base_url().'index.php/auth/logout"><span class="font-icon glyphicon glyphicon-log-out"></span>Logout</a>
								</div>
							</div>
						</div><!--.site-header-shown-->
					</div><!--site-header-content-in-->
				</div><!--.site-header-content-->
			</div><!--.container-fluid-->
		</header><!--.site-header-->
		<div class="mobile-menu-left-overlay"></div>
		<nav class="side-menu">
			<div class="side-menu-avatar">
				<div class="avatar-preview avatar-preview-100">
					<img src="'.$current_user->avatar.'" alt="">
				</div>
			</div>
			<ul class="side-menu-list">
				<li class="blue">
					<a href="'.site_url('auth').'">
						<i class="font-icon font-icon-home"></i>
						<span class="lbl">Beranda</span>
					</a>
				</li>
				<li class="blue with-sub">
					<span>
						<i class="font-icon font-icon-users"></i>
						<span class="lbl">Pengguna</span>
					</span>
					<ul>
						<li><a href="'.base_url().'index.php/auth"><span class="lbl">Pengguna Terdaftar</span></a></li>
						<li><a href="'.base_url().'index.php/auth/create_user"><span class="lbl">Pengguna Baru</span></a></li>
						<li><a href="'.base_url().'index.php/kelas"><span class="lbl">Kelas</span></a></li>
						
					</ul>
				</li>
				<li class="blue with-sub">
					<span>
						<i class="fa fa-question-circle"></i>
						<span class="lbl">Pengelolaan Kuis</span>
					</span>
					<ul>
						<li><a href="'.base_url().'index.php/Quiz_develop"><span class="lbl">Daftar Kuis</span></a></li>
						<li><a href="'.base_url().'index.php/Quiz_question_item"><span class="lbl">Kumpulan Soal</span></a></li>
						<li><a href="'.base_url().'index.php/Quiz_score"><span class="lbl">Skor</span></a></li>
					</ul>
				</li>
			</ul>
		</nav>
		';
	}
	public static function extra()
	{
		echo '
		
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/tether/tether.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/bootstrap/bootstrap.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/plugins.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/select2/select2.full.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/jquery-tag-editor/jquery.caret.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/jquery-tag-editor/jquery.tag-editor.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/bootstrap-select/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/match-height/jquery.matchHeight.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/slick-carousel/slick.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/html5-form-validation/jquery.validation.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/input-mask/jquery.mask.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/lib/input-mask/input-mask-init.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
		<script src="'; echo base_url(); echo 'templates/StartUI/build/js/app.js"></script>
		
		<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

		';
		?>
		<script>
		$(document).ready(function(){
			$('.inputDate').datepicker({
				autoclose: true,
				format: 'yyyy-mm-dd'
			});

			$('.inputDate').on('show', function(e){
				console.debug('show', e.date, $(this).data('stickyDate'));
				
				if ( e.date ) {
					 $(this).data('stickyDate', e.date);
				}
				else {
					 $(this).data('stickyDate', null);
				}
			});

			$('.inputDate').on('hide', function(e){
				console.debug('hide', e.date, $(this).data('stickyDate'));
				var stickyDate = $(this).data('stickyDate');
				
				if ( !e.date && stickyDate ) {
					console.debug('restore stickyDate', stickyDate);
					$(this).datepicker('setDate', stickyDate);
					$(this).data('stickyDate', null);
				}
			});
			$("#upload_avatar").change(function(){
				picture(this);
			});
			var picture_width;
			var picture_height;
			var crop_max_width = 300;
			var crop_max_height = 300;
			function picture(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();
					reader.onload = function (e) {
						$("#jcrop, #preview").html("").append("<img src=\""+e.target.result+"\" alt=\"\" />");
						picture_width = $("#preview img").width();
						picture_height = $("#preview img").width();
						$("#jcrop  img").Jcrop({
							onChange: canvas,
							onSelect: canvas,
							aspectRatio: 1,
							boxWidth: crop_max_width,
							boxHeight: crop_max_height,
						});
					}
					reader.readAsDataURL(input.files[0]);
				}
			}
			function canvas(coords){
				var imageObj = $("#jcrop img")[0];
				var canvas = $("#canvas")[0];
				canvas.width  = coords.w;
				canvas.height = coords.h;
				var context = canvas.getContext("2d");
				context.drawImage(imageObj, coords.x, coords.y, coords.w, coords.h, 0, 0, canvas.width, canvas.height);
				png();
			}
			function png() {
				var png = $("#canvas")[0].toDataURL('image/png');
				$("#avatar").val(png);
			}
			var options = {
			  valueNames: [ 'fname', 'lname', 'acname' ],
			  page: 20,
  			  pagination: true
			};

			var userList = new List('users', options);

			setInterval(function(){
				$('.pagination > li').addClass('btn btn-default-outline');
			},0);
		});
		</script>
		<?php
		echo '
		</body>
		';
	}
	public static function footer()
	{
		echo '</html>';
	}
	
	
	
	public static function auth_header()
	{
		echo '
		
		<!DOCTYPE html>
		<html>
		<head lang="en">
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
			<meta http-equiv="x-ua-compatible" content="ie=edge">
			<title>FarahQuiz</title>

			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="apple-touch-icon" type="image/png">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/logofix.png" rel="icon" type="image/png">
			<link href="'; echo base_url(); echo 'templates/StartUI/build/img/favicon.ico" rel="shortcut icon">

			<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			<![endif]-->
			<link rel="stylesheet" href="';echo base_url();echo 'templates/StartUI/build/css/separate/pages/login.min.css">
			<link rel="stylesheet" href="';echo base_url();echo 'templates/StartUI/build/css/lib/font-awesome/font-awesome.min.css">
			<link rel="stylesheet" href="';echo base_url();echo 'templates/StartUI/build/css/lib/bootstrap/bootstrap.min.css">
			<link rel="stylesheet" href="';echo base_url();echo 'templates/StartUI/build/css/main.css">
		</head>
		
		';
	}
	public static function auth_sidebar()
	{
		echo '
		
		<body>
		
		';
	}
	public static function auth_extra()
	{
		echo '
		
		<script src="';echo base_url();echo 'templates/StartUI/build/js/lib/jquery/jquery.min.js"></script>
		<script src="';echo base_url();echo 'templates/StartUI/build/js/lib/tether/tether.min.js"></script>
		<script src="';echo base_url();echo 'templates/StartUI/build/js/lib/bootstrap/bootstrap.min.js"></script>
		<script src="';echo base_url();echo 'templates/StartUI/build/js/plugins.js"></script>
			<script type="text/javascript" src="';echo base_url();echo 'templates/StartUI/build/js/lib/match-height/jquery.matchHeight.min.js"></script>
			<script>
				$(function() {
					$(".page-center").matchHeight({
						target: $("html")
					});

					$(window).resize(function(){
						setTimeout(function(){
							$(".page-center").matchHeight({ remove: true });
							$(".page-center").matchHeight({
								target: $("html")
							});
						},100);
					});
				});
			</script>
		<script src="';echo base_url();echo 'templates/StartUI/build/js/app.js"></script>
		</body>
		
		';
	}
	public static function auth_footer()
	{
		echo '
		
		</html>
		
		';
	}
}
