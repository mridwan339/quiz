<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quiz_Develop extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        
        $this->load->helper(array('url','language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        
        $this->lang->load('auth');
        $this->load->model('Quiz_question_model');
        $this->load->model('Quiz_question_item_model');
        $this->load->model('Quiz_Develop_Model');
        require_once('Template.php');
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }
        else
        {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            
            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            $this->data['groups'] = $this->ion_auth->groups()->result();
            foreach ($this->data['users'] as $k => $user)
            {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }
        }

        
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'quiz_question/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'quiz_question/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'quiz_question/';
            $config['first_url'] = base_url() . 'quiz_question/';
        }

        $config['per_page'] = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Quiz_Develop_Model->total_rows($q);
        //$quiz_question = $this->Quiz_Develop_Model->get_limit_data($config['per_page'], $start, $q);
        $quiz_question = $this->Quiz_Develop_Model->get_all();
        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'quiz_question_data' => $quiz_question,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('quiz_develop/quiz_develop_list', $data);
    }

    public function read($id){
        $row = $this->Quiz_Develop_Model->get_by_id($id);
        $jumlah_soal = $row[0]->q_total;
        if($jumlah_soal<=count($row)){
            $status=1;
        }else{
            $status=0;
        }
        $data["isi"] = $row;
        $data['id'] = $id;
        $data["status"] = $status;
        //var_dump($jumlah_soal);
        $this->load->view('quiz_develop/quiz_develop_read',$data);
    }

    public function preview($id) 
    {
        $row = $this->Quiz_question_item_model->get_by_id($id);
        if ($row) {
            $data = array(
        'q_itemid' => $row->q_itemid,
        'q_question_id' => $row->q_question_id,
        'q_title' => $row->q_title,
        'q_no_question' => $row->q_no_question,
        'q_question_image' => $row->q_question_image,
        'q_question' => $row->q_question,
        'q_multipleChoices' => $row->q_multipleChoices,
        'q_answerkey' => $row->q_answerkey,
        'q_description' => $row->q_description,
        );
            $this->load->view('quiz_develop_item/quiz_develop_item_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Data Tidak Ditemukan');
            redirect(site_url('quiz_question_item'));
        }
    }

    public function reads($id) 
    {
        $row = $this->Quiz_question_item_model->get_by_id($id);
        if ($row) {
            $data = array(
        'q_itemid' => $row->q_itemid,
        'q_question_id' => $row->q_question_id,
        'q_title' => $row->q_title,
        'q_no_question' => $row->q_no_question,
        'q_question_image' => $row->q_question_image,
        'q_question' => $row->q_question,
        'q_multipleChoices' => $row->q_multipleChoices,
        'q_answerkey' => $row->q_answerkey,
        'q_description' => $row->q_description,
        );
            $this->load->view('quiz_develop_item/quiz_question_item_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Data Tidak Ditemukan');
            redirect(site_url('quiz_develop/read/'));
        }
    }

    public function create($id=NULL) 
    {
        if($id){
        $qq  = $this->Quiz_question_model->get_by_id($id);

        
        $data = array(
            'button' => 'Buat',
            'action' => site_url('Quiz_Develop/create_action/'),
        'q_itemid' => $id,
        'q_question_id' => set_value('q_question_id'),
        'q_no_question' => set_value('q_no_question'),
        'q_question_image' => set_value('q_question_image'),
        'q_question' => set_value('q_question'),
        'q_multipleChoices' => set_value('q_multipleChoices'),
        'q_answerkey' => set_value('q_answerkey'),
        'q_description' => set_value('q_description'),
        'quiz'=>$this->Quiz_question_model->get_all(),
        'Cquiz_typeAlpha'=>$qq->q_type_alpha,
        'idQuiz'=>$id,
    );
    }else{
        
        $data = array(
            'button' => 'Buat',
            'action' => site_url('Quiz_Develop/create_action/'.$id),
        'q_itemid' => $id,
        'q_question_id' => set_value('q_question_id'),
        'q_no_question' => set_value('q_no_question'),
        'q_question_image' => set_value('q_question_image'),
        'q_question' => set_value('q_question'),
        'q_multipleChoices' => set_value('q_multipleChoices'),
        'q_answerkey' => set_value('q_answerkey'),
        'q_description' => set_value('q_description'),
        'quiz'=>$this->Quiz_question_model->get_all(),
        'Cquiz_typeAlpha'=>"",
        'idQuiz'=>"",
        
    );
    }
        $this->load->view('quiz_develop/quiz_develop_form', $data);
    }

    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
        'q_question_id' => $this->input->post('q_question_id',TRUE),
        'q_no_question' => $this->input->post('q_no_question',TRUE),
        'q_question_image' => $this->input->post('q_question_image'),
        'q_question' => $this->input->post('q_question',TRUE),
        'q_multipleChoices' => $this->input->post('q_multipleChoices',TRUE),
        'q_answerkey' => $this->input->post('q_answerkey',TRUE),
        'q_description' => $this->input->post('q_description',TRUE),
        );

            $this->Quiz_question_item_model->insert($data);
            //echo $this->input->post('q_question_id',TRUE);
            //var_dump($_POST);
            $this->session->set_flashdata('message', 'Data Berhasil Ditambahkan');
            redirect(site_url('Quiz_Develop/read/'.$this->input->post('q_itemid')));
        }
    }

    public function update($id) 
    {
        $row = $this->Quiz_question_item_model->get_by_id($id);
        $qq  = $this->Quiz_question_model->get_by_id($row->q_question_id);
        
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('quiz_develop/update_action'),
        'q_itemid' => set_value('q_itemid', $row->q_itemid),
        'q_question_id' => set_value('q_question_id', $row->q_question_id),
        'q_no_question' => set_value('q_no_question', $row->q_no_question),
        'q_question_image' => set_value('q_question_image', $row->q_question_image),
        'q_question' => set_value('q_question', $row->q_question),
        'q_multipleChoices' => set_value('q_multipleChoices', $row->q_multipleChoices),
        'q_answerkey' => set_value('q_answerkey', $row->q_answerkey),
        'q_description' => set_value('q_description', $row->q_description),
        'quiz'=>$this->Quiz_question_model->get_all(),
        'Cquiz_typeAlpha'=>$qq->q_type_alpha,
        );
            $this->load->view('quiz_develop_item/quiz_develop_item_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Data Tidak Ditemukan');
            redirect(site_url('quiz_develop'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('q_itemid', TRUE));
        } else {
            $data = array(
        'q_question_id' => $this->input->post('q_question_id',TRUE),
        'q_no_question' => $this->input->post('q_no_question',TRUE),
        'q_question_image' => $this->input->post('q_question_image'),
        'q_question' => $this->input->post('q_question',TRUE),
        'q_multipleChoices' => $this->input->post('q_multipleChoices',TRUE),
        'q_answerkey' => $this->input->post('q_answerkey',TRUE),
        'q_description' => $this->input->post('q_description',TRUE),
        );

            $this->Quiz_question_item_model->update($this->input->post('q_itemid', TRUE), $data);
            $this->session->set_flashdata('message', 'Data Berhasil Diperbarui');
            //redirect(site_url('quiz_develop'));
            redirect(site_url('Quiz_develop/read/'.$this->input->post('q_question_id')));
        }
    }

    public function delete($id) 
    {
        $row = $this->Quiz_question_item_model->get_by_id($id);

        if ($row) {
            $data = array(
                $quest => $row->q_question_id);
            $this->Quiz_question_item_model->delete($id);
            $this->session->set_flashdata('message', 'Data Berhasil Dihapus');
            redirect(site_url('Quiz_develop'));
            //redirect(site_url('quiz_develop'));
            //javascript:history.back();
        } else {
            $this->session->set_flashdata('message', 'Data Tidak Ditemukan');
            redirect(site_url('quiz_develop'));
        }
    }

    

    public function _rules() 
    {
	$this->form_validation->set_rules('q_question_id', 'q question id', 'trim|required');
	$this->form_validation->set_rules('q_no_question', 'q no question', 'trim|required');
	$this->form_validation->set_rules('q_question', 'q question', 'trim|required');
	$this->form_validation->set_rules('q_multipleChoices', 'q multiplechoices', 'trim|required');
	$this->form_validation->set_rules('q_answerkey', 'q answerkey', 'trim|required');
	$this->form_validation->set_rules('q_description', 'q description', 'trim|required');

	$this->form_validation->set_rules('q_itemid', 'q_itemid', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }} ?>