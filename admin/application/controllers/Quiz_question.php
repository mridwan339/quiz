<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quiz_question extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        
        $this->load->helper(array('url','language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        
        $this->lang->load('auth');
        $this->load->model('Quiz_question_model');
        require_once('Template.php');
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }
        else
        {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            
            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            $this->data['groups'] = $this->ion_auth->groups()->result();
            foreach ($this->data['users'] as $k => $user)
            {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }
        }

        
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'quiz_question/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'quiz_question/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'quiz_question/';
            $config['first_url'] = base_url() . 'quiz_question/';
        }

        $config['per_page'] = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Quiz_question_model->total_rows($q);
        $quiz_question = $this->Quiz_question_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'quiz_question_data' => $quiz_question,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('quiz_question/quiz_question_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Quiz_question_model->get_by_id($id);
        if ($row) {
            $data = array(
		'q_id' => $row->q_id,
		'q_creator_id' => $row->q_creator_id,
        'username' => $row->username,
		'q_total' => $row->q_total,
		'q_type_alpha' => $row->q_type_alpha,
		'q_kind_feed' => $row->q_kind_feed,
		'q_cover' => $row->q_cover,
		'q_title' => $row->q_title,
		'q_time' => $row->q_time,
		'q_post_publish_date' => $row->q_post_publish_date,
		'q_post_expired' => $row->q_post_expired,
	    );
            $this->load->view('quiz_question/quiz_question_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('quiz_question'));
        }
    }

    public function create() 
    {
        $users=$this->ion_auth->users()->result();
        $user_id=$this->ion_auth->get_user_id();
        $user_group=$this->ion_auth->get_users_groups()->row()->id;
        $optionAdmin="";
        foreach ($users as $user) {
            $user_group_rows=$this->ion_auth->get_users_groups($user->id)->row()->id;
            if($user_group_rows==1){
                    
                    $optionAdmin .='<option value="'.$user->id.'" data-photo="'.$user->avatar.'">'.$user->first_name.' '.$user->last_name.'</option>';
            }
        }


        $data = array(
            'button' => 'Buat',
            'action' => site_url('quiz_question/create_action'),
	    'q_id' => set_value('q_id'),
	    'q_creator_id' => set_value('q_creator_id'),
	    'q_total' => set_value('q_total'),
	    'q_type_alpha' => set_value('q_type_alpha'),
	    'q_kind_feed' => set_value('q_kind_feed'),
	    'q_cover' => set_value('q_cover'),
	    'q_title' => set_value('q_title'),
	    'q_time' => set_value('q_time'),
        // 'users'=>$this->ion_auth->users()->result(),
        'users'=>$users,
        'user_id'=>$user_id,
        'user_group'=>$user_group,
        'optionAdmin'=>$optionAdmin,
	    'q_post_publish_date' => set_value('q_post_publish_date'),
	    'q_post_expired' => set_value('q_post_expired'),
	);
        $this->load->view('quiz_question/quiz_question_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'q_creator_id' => $this->input->post('q_creator_id',TRUE),
		'q_total' => $this->input->post('q_total',TRUE),
		'q_type_alpha' => $this->input->post('q_type_alpha',TRUE),
		'q_kind_feed' => $this->input->post('q_kind_feed',TRUE),
		'q_cover' => $this->input->post('q_cover'),
		'q_title' => $this->input->post('q_title',TRUE),
		'q_time' => $this->input->post('q_time',TRUE),
		'q_post_publish_date' => $this->input->post('q_post_publish_date',TRUE),
		'q_post_expired' => $this->input->post('q_post_expired',TRUE),
	    );

            $this->Quiz_question_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('quiz_develop'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Quiz_question_model->get_by_id($id);
        $users=$this->ion_auth->users()->result();
        $user_id=$this->ion_auth->get_user_id();
        $user_group=$this->ion_auth->get_users_groups()->row()->id;
        $optionAdmin="";
        foreach ($users as $user) {
            $user_group_rows=$this->ion_auth->get_users_groups($user->id)->row()->id;
            if($user_group_rows==1){
                    
                    $optionAdmin .='<option value="'.$user->id.'" data-photo="'.$user->avatar.'">'.$user->first_name.' '.$user->last_name.'</option>';
            }
        }

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('quiz_question/update_action'),
		'q_id' => set_value('q_id', $row->q_id),
		'q_creator_id' => set_value('q_creator_id', $row->q_creator_id),
		'q_total' => set_value('q_total', $row->q_total),
		'q_type_alpha' => set_value('q_type_alpha', $row->q_type_alpha),
		'q_kind_feed' => set_value('q_kind_feed', $row->q_kind_feed),
		'q_cover' => set_value('q_cover', $row->q_cover),
		'q_title' => set_value('q_title', $row->q_title),
        // 'users'=>$this->ion_auth->users()->result(),
        'users'=>$users,
        'user_id'=>$user_id,
        'user_group'=>$user_group,
        'optionAdmin'=>$optionAdmin,
		'q_time' => set_value('q_time', $row->q_time),
		'q_post_publish_date' => set_value('q_post_publish_date', $row->q_post_publish_date),
		'q_post_expired' => set_value('q_post_expired', $row->q_post_expired),
	    );
            $this->load->view('quiz_question/quiz_question_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('quiz_develop'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('q_id', TRUE));
        } else {
            $data = array(
		'q_creator_id' => $this->input->post('q_creator_id',TRUE),
		'q_total' => $this->input->post('q_total',TRUE),
		'q_type_alpha' => $this->input->post('q_type_alpha',TRUE),
		'q_kind_feed' => $this->input->post('q_kind_feed',TRUE),
		'q_cover' => $this->input->post('q_cover'),
		'q_title' => $this->input->post('q_title',TRUE),
		'q_time' => $this->input->post('q_time',TRUE),
		'q_post_publish_date' => $this->input->post('q_post_publish_date',TRUE),
		'q_post_expired' => $this->input->post('q_post_expired',TRUE),
	    );

            $this->Quiz_question_model->update($this->input->post('q_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('quiz_develop'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Quiz_question_model->get_by_id($id);

        if ($row) {
            $this->Quiz_question_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('quiz_develop'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('quiz_develop'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('q_creator_id', 'q creator id', 'trim|required');
	$this->form_validation->set_rules('q_total', 'q total', 'trim|required');
	$this->form_validation->set_rules('q_type_alpha', 'q type alpha', 'trim|required');
	$this->form_validation->set_rules('q_kind_feed', 'q kind feed', 'trim|required');
	$this->form_validation->set_rules('q_cover', 'q cover', 'trim|required');
	$this->form_validation->set_rules('q_title', 'q title', 'trim|required');
	$this->form_validation->set_rules('q_time', 'q time', 'trim|required');
	$this->form_validation->set_rules('q_post_publish_date', 'q post publish date', 'trim|required');
	$this->form_validation->set_rules('q_post_expired', 'q post expired', 'trim|required');

	$this->form_validation->set_rules('q_id', 'q_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }} ?>