<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quiz_score extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        
        $this->load->helper(array('url','language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        
        $this->lang->load('auth');
        $this->load->model('Quiz_score_model');
        $this->load->model('Quiz_question_model');
        require_once('Template.php');
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            // redirect them to the home page because they must be an administrator to view this
            return show_error('You must be an administrator to view this page.');
        }
        else
        {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            
            //list the users
            $this->data['users'] = $this->ion_auth->users()->result();
            $this->data['groups'] = $this->ion_auth->groups()->result();
            foreach ($this->data['users'] as $k => $user)
            {
                $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }
        }

        
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'quiz_score/?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'quiz_score/?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'quiz_score/';
            $config['first_url'] = base_url() . 'quiz_score/';
        }

        $config['per_page'] = 20;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Quiz_score_model->total_rows($q);
        $quiz_score = $this->Quiz_score_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'quiz_score_data' => $quiz_score,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('quiz_score/quiz_score_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Quiz_score_model->get_by_id($id);
        if ($row) {
            $data = array(
		'q_idscore' => $row->q_idscore,
		'q_id_user' => $row->q_id_user,
		'q_id_question' => $row->q_id_question,
		'q_score' => $row->q_score,
		'q_date' => $row->q_date,
        'username' => $row->username,
        'q_title' => $row->q_title,
	    );
            $this->load->view('quiz_score/quiz_score_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('quiz_score'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('quiz_score/create_action'),
	    'q_idscore' => set_value('q_idscore'),
	    'q_id_user' => set_value('q_id_user'),
	    'q_id_question' => set_value('q_id_question'),
	    'q_score' => set_value('q_score'),
	    'q_date' => set_value('q_date'),
        'quiz'=>$this->Quiz_question_model->get_all(),
        'users'=>$this->ion_auth->users()->result(),
	);
        $this->load->view('quiz_score/quiz_score_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'q_id_user' => $this->input->post('q_id_user',TRUE),
		'q_id_question' => $this->input->post('q_id_question',TRUE),
		'q_score' => $this->input->post('q_score',TRUE),
		'q_date' => $this->input->post('q_date',TRUE),
	    );

            $this->Quiz_score_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('quiz_score'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Quiz_score_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('quiz_score/update_action'),
		'q_idscore' => set_value('q_idscore', $row->q_idscore),
		'q_id_user' => set_value('q_id_user', $row->q_id_user),
		'q_id_question' => set_value('q_id_question', $row->q_id_question),
		'q_score' => set_value('q_score', $row->q_score),
		'q_date' => set_value('q_date', $row->q_date),
        'quiz'=>$this->Quiz_question_model->get_all(),
        'users'=>$this->ion_auth->users()->result(),
	    );
            $this->load->view('quiz_score/quiz_score_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('quiz_score'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('q_idscore', TRUE));
        } else {
            $data = array(
		'q_id_user' => $this->input->post('q_id_user',TRUE),
		'q_id_question' => $this->input->post('q_id_question',TRUE),
		'q_score' => $this->input->post('q_score',TRUE),
		'q_date' => $this->input->post('q_date',TRUE),
	    );

            $this->Quiz_score_model->update($this->input->post('q_idscore', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('quiz_score'));
        }
    }

    public function print_data(){
        $score = $this->Quiz_score_model->get_all();
        echo '
        <table border="1" style="width:100%;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Quiz</th>
                    <th>Score</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>';
            $no=0;
            foreach ($score as $res) {
            $no++;    
                echo'
                <tr>
                    <td>'.$no.'</td>
                    <td>'.$res->username.'</td>
                    <td>'.$res->q_title.'</td>
                    <td>'.$res->q_score.'</td>
                    <td>'.$res->q_date.'</td>
                </tr>
                ';
            }

            echo'</tbody>
        </table>
        ';
        ?>
        <script type="text/javascript">
        window.print();
        </script>
        <?php
    }
    
    public function delete($id) 
    {
        $row = $this->Quiz_score_model->get_by_id($id);

        if ($row) {
            $this->Quiz_score_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('quiz_score'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('quiz_score'));
        }
    }



    public function _rules() 
    {
	$this->form_validation->set_rules('q_id_user', 'q id user', 'trim|required');
	$this->form_validation->set_rules('q_id_question', 'q id question', 'trim|required');
	$this->form_validation->set_rules('q_score', 'q score', 'trim|required');
	$this->form_validation->set_rules('q_date', 'q date', 'trim|required');

	$this->form_validation->set_rules('q_idscore', 'q_idscore', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }} ?>