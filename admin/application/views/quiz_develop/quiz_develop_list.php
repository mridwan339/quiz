<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
        <h2 style="margin-top:0px;">Daftar Quiz Dari Quiz Develop</h2>
        <section class="box-typical">
        <div style="margin-bottom: 10px">
            <header class="box-typical-header">

                <div style="display: table-caption;">
                <form action="<?php echo site_url('quiz_question/index'); ?>" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('quiz_question'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Cari</button>
                        </span>
                    </div>
                </form>
                <div style=" clear:both; "></div>
                </div>


                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title">
                        <h3><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : 'Di Bawah ini adalah Daftar Kuis'; ?></h3>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <button type="button" onclick="window.location='<?php echo site_url('Quiz_Question/create'); ?>'" class="action-btn"><i class="fa fa-plus-circle"></i></button>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <b><?php echo 'Quiz'; ?></b>
                    </div>
                </div>
            </header>
            <div class="box-typical-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                <th>No</th>
        		
        		<th>Title</th>
                <th>Tipe Soal</th>
        		<th>Publish</th>
        		<th>Expired</th>
                <th>Soal</th>
        		<th>Action</th>
            </tr>
                        </thead>
                        <tbody>
                            <?php
            foreach ($quiz_question_data as $quiz_question)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $quiz_question->q_title ?></td>
            <td><?php echo $quiz_question->q_kind_feed ?></td>
			<td><?php echo date("d-m-Y",strtotime($quiz_question->q_post_publish_date)); ?></td>
			<td><?php echo date("d-m-Y",strtotime($quiz_question->q_post_expired)); ?></td>
            <td><a href="<?php echo site_url('Quiz_Develop/read/'.$quiz_question->q_id); ?>"><button type="button" class="btn btn-primary-outline">Soal</button></a></td>
			
<td>
<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
    
    <a href="<?php echo site_url('quiz_question/update/'.$quiz_question->q_id); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-edit "></i></button></a>
    <a href="<?php echo site_url('quiz_question/delete/'.$quiz_question->q_id); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-trash "></i></button></a>
    <!-- <a href="<?php //echo site_url(//'Quiz_Develop/read/'.$quiz_question->q_id); ?>"><button type="button" class="btn btn-default-outline">Soal</i></button></a> -->
</div>

			</td>
		</tr>
                <?php
            }
            ?>
                        </tbody>
                    </table>
                </div>
            </div>




            
    </section>
    <div class="row">
            <div class="col-md-6">
                <a href="#" style=" margin-bottom:10px; " class="btn btn-primary">Jumlah Paket : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>