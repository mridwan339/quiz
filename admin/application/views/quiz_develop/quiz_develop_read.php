<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
    	<h2 style="margin-top:0px;">Pertanyaan Untuk Quiz</h2>
    	 <section class="box-typical">
        	<div style="margin-bottom: 10px">
                <?php
                    if($status!=0){
                ?>
                    <header class="box-typical-header">
                        <div class="tbl-cell tbl-cell-title">
                            <h3>Jumlah Quiz Sudah Maksimal</h3>
                        </div>
                    </header>
                <?php
                    }else{
                ?>
                    <header class="box-typical-header">
                        <div class="tbl-row">
                            <div class="tbl-cell tbl-cell-title">
                                <h3><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : 'Di Bawah ini adalah Daftar Kuis'; ?></h3>
                            </div>
                            <div class="tbl-cell tbl-cell-action-bordered">
                                <button type="button" onclick="window.location='<?php echo site_url('Quiz_Develop/create/'.$id); ?>'" class="action-btn"><i class="fa fa-plus-circle"></i></button>
                            </div>
                            <div class="tbl-cell tbl-cell-action-bordered">
                                <b><?php echo 'Quiz'; ?></b>
                            </div>
                        </div>
                    </header>
                <?php 
                    }
                ?>
            	<div class="box-typical-body">
                	<div class="table-responsive">
                    	<table class="table table-hover">
                        	<thead>
                        		<tr>
                        			<th>No</th>
                        			<th>Nama Quiz</th>
                        			<th>Pertanyaan Quiz</th>
                                    <th>Jawaban</th>
                                    <th>Aksi</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                        		<?php
                        			foreach ($isi as $key => $value) {
                        		?>
                        			<tr>
                        				<th><?=$key+1?></th>
                        				<th><?=$value->q_title?></th>
                        				<th><?=$value->q_question?></th>
                                        <th><?=$value->q_answerkey?></th>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
                                                <a href="<?php echo site_url('quiz_develop/preview/'.$value->q_itemid); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-eye-open "></i></button></a>
                                                <a href="<?php echo site_url('quiz_develop/update/'.$value->q_itemid); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-edit "></i></button></a>
                                                <a href="<?php echo site_url('quiz_develop/delete/'.$value->q_itemid); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-trash "></i></button></a>
                                                <!-- <a href="<?php //echo site_url(//'Quiz_Develop/read/'.$quiz_question->q_id); ?>"><button type="button" class="btn btn-default-outline">Soal</i></button></a> -->
                                            </div>
                                        </td>
                        			</tr>
                                    
                        		<?php
                        			}
                        		?>
                        	</tbody>
                        </table>
                    </div>
                </div>
            </div>
         </section>

         <a href="<?php echo site_url('Quiz_develop') ?>" class="btn btn-primary-outline">Kembali ke Quiz</a>     
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>