<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
    
        <h2 style="margin-top:0px"> <?php if($button=="Update"){ echo "Edit"; }else{ echo "New"; } ?> Groups</h2>
        <section class="box-typical card-block">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="varchar">Nama <?php echo form_error('name') ?></label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control" name="name" id="name" placeholder="Nama" <?php echo $name=="member" ? 'readonly=readonly':''; echo $name=="admin" ? 'readonly=readonly':''; ?> value="<?php echo $name; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="varchar">Deskripsi <?php echo form_error('description') ?></label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control" name="description" id="description" placeholder="Deskripsi" <?php echo $description=="member" ? 'readonly=readonly':''; echo $description=="admin" ? 'readonly=readonly':''; ?> value="<?php echo $description; ?>"></p>
        </div>
    </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('groups') ?>" class="btn btn-danger">Batal</a>
	</form>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>