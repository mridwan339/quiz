<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
        <h2 style="margin-top:0px">Detail Groups</h2>
        <section class="box-typical card-block">
        <table class="table">
	    <tr><td style='border-top: none !important;'>Name</td><td style='border-top: none !important;'><?php echo $name; ?></td></tr>
	    <tr><td >Description</td><td ><?php echo $description; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('groups') ?>" class="btn btn-default">Back to List</a></td></tr>
	</table>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>