<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
        <h2 style="margin-top:0px;">Daftar Item Quiz</h2>
        <section class="box-typical">
        <div style="margin-bottom: 10px">
            <header class="box-typical-header">

                <div style="display: table-caption;">
                <form action="<?php echo site_url('quiz_question_item/index'); ?>" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('quiz_question_item'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Cari</button>
                        </span>
                    </div>
                </form>
                <div style=" clear:both; "></div>
                </div>


                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title">
                        <h3><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : 'Di bawah ini list dari Quiz Item.'; ?></h3>
                    </div>
                    <!-- <div class="tbl-cell tbl-cell-action-bordered">
                        <button type="button" onclick="window.location='<?php //echo site_url('quiz_question_item/create'); ?>'" class="action-btn"><i class="fa fa-plus-circle"></i></button>
                    </div> -->
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <b><?php echo 'Quiz Items'; ?></b>
                    </div>
                    
                </div>
            </header>
            <div class="box-typical-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                <th>No</th>
		<th>Nama Quiz</th>
		<th>Soal No</th>
        <th>Soal</th>
        <th>Jawaban</th>
		<!-- <th>Deskripsi</th> -->
		<th>Aksi</th>
            </tr>
                        </thead>
                        <tbody>
                            <?php
            foreach ($quiz_question_item_data as $quiz_question_item)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $quiz_question_item->q_title ?></td>
			<td><?php echo $quiz_question_item->q_no_question ?></td>
            <td><?php echo $quiz_question_item->q_question ?></td>
            <td><?php echo $quiz_question_item->q_answerkey ?></td>
			<!-- <td><?php //echo $quiz_question_item->q_description ?></td> -->
			
<td style="text-align:center" width="200px">
<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
    <a href="<?php echo site_url('quiz_question_item/read/'.$quiz_question_item->q_itemid); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-eye-open "></i></button></a>
    <a href="<?php echo site_url('quiz_question_item/update/'.$quiz_question_item->q_itemid); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-edit "></i></button></a>
    <a href="<?php echo site_url('quiz_question_item/delete/'.$quiz_question_item->q_itemid); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-trash "></i></button></a>
</div>

			</td>
		</tr>
                <?php
            }
            ?>
                        </tbody>
                    </table>
                </div>
            </div>




            
    </section>
    <div class="row">
            <div class="col-md-6">
                <a href="#" style=" margin-bottom:10px; " class="btn btn-primary">Jumlah Item Soal : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>