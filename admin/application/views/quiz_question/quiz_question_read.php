<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
        <h2 style="margin-top:0px">Detail Quiz</h2>
        <section class="box-typical card-block">
        <table class="table">
	    <tr><td style='border-top: none !important;'>Creator</td><td style='border-top: none !important;'><?php echo $username; ?></td></tr>
	    <tr><td >Jumlah Soal</td><td ><?php echo $q_total; ?> Soal</td></tr>
	    <tr><td >Tipe Jawaban</td><td ><?php echo $q_type_alpha; ?></td></tr>
	    <tr><td >Tipe Feedback</td><td ><?php echo $q_kind_feed; ?></td></tr>
	    <tr><td >Cover</td><td ><img src="<?php echo $q_cover; ?>" width="100%"></td></tr>
	    <tr><td >Nama Paket Soal</td><td ><?php echo $q_title; ?></td></tr>
	    <tr><td >Waktu (menit)</td><td ><?php echo $q_time; ?></td></tr>
	    <tr><td >Tanggal Rilis Soal</td><td ><?php echo date($q_post_publish_date); ?></td></tr>
	    <tr><td >Masa Kadaluarsa</td><td ><?php echo $q_post_expired; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('quiz_question') ?>" class="btn btn-default">Kembali ke List</a></td></tr>
	</table>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>