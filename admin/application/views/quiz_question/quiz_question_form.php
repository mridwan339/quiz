<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
    
        <h2 style="margin-top:0px"> <?php if($button=="Update"){ echo "Edit"; }else{ echo "New"; } ?> Quiz</h2>
		<?php echo form_error('q_creator_id'); ?>
        <?php echo form_error('q_total'); ?>
        <?php echo form_error('q_type_alpha'); ?>
        <?php echo form_error('q_kind_feed'); ?>
        <?php echo form_error('q_cover'); ?>
        <?php echo form_error('q_title'); ?>
        <?php echo form_error('q_time'); ?>
        <?php echo form_error('q_post_publish_date'); ?>
        <?php echo form_error('q_post_expired'); ?>
        <section class="box-typical card-block">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="int">Creator</label>
        <div class="col-sm-10">
            <?php
            /*
            ?>
            <p class="form-control-static"><input class="form-control" name="q_creator_id" id="q_creator_id" placeholder="Q Creator Id" value="<?php echo $q_creator_id; ?>"></p>
            <?php
            */
            ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_creator_id !=="" ){
                        echo "<script>
                            var creatorID='$q_creator_id';
                        </script>";
                    }else{
                        echo "<script>
                            
                        </script>";
                    } ?>
                    <!-- var creatorID=''; -->
                    <!--yang ini-->
                    <select name="q_creator_id" id="creatorID" class="select2 select2-photo">
                        <?php echo $optionAdmin; ?>
                        <!--
                        <?php //foreach ($users as $user):?>
                            <option value="<?php //echo $user->id; ?>" data-photo="<?php //echo $user->avatar; ?>"><?php //echo $user->first_name." ".$user//->last_name; ?></option>
                        <?php //endforeach;?>
                        <!--<option data-photo="../../../templates/StartUI/build/img/photo-64-1.jpg">Jenny Hess</option>
                        <option data-photo="../../../templates/StartUI/build/img/photo-64-2.jpg">Elliot Fu</option>
                        <option data-photo="../../../templates/StartUI/build/img/photo-64-3.jpg">Stevie Felicano</option>
                        <option data-photo="../../../templates/StartUI/build/img/photo-64-4.jpg">Christian</option>-->
                    </select>
                </div>
            </div><!--.row-->
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="int">Jumlah Soal</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input type="number" class="form-control" name="q_total" id="q_total" placeholder="Jumlah Soal" value="<?php echo $q_total; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="enum">Tipe Jawaban</label>
        <div class="col-sm-10">
            <?php /* ?>
            <p class="form-control-static"><input class="form-control" name="q_type_alpha" id="q_type_alpha" placeholder="Q Type Alpha" value="<?php echo $q_type_alpha; ?>"></p>
            <?php */ ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_type_alpha !=="" ){
                        echo "<script>
                            var q_type_alpha='$q_type_alpha';
                        </script>";
                    }else{
                        echo "<script>
                            var q_type_alpha='';
                        </script>";
                    } ?>
                    <!--yang ini-->
                    <select name="q_type_alpha" id="q_type_alpha" class="select2">
                            <option value="abc">(3) A , B , C</option>
                            <option value="abcd">(4) A , B , C , D</option>
                            <option value="abcde">(5) A , B , C , D , E</option>
                    </select>
                </div>
            </div><!--.row-->
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="enum">Tipe Feedback</label>
        <div class="col-sm-10">
            <?php /* ?>
            <p class="form-control-static"><input class="form-control" name="q_kind_feed" id="q_kind_feed" placeholder="Q Kind Feed" value="<?php echo $q_kind_feed; ?>"></p>
            <?php */ ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_kind_feed !=="" ){
                        echo "<script>
                            var q_kind_feed='$q_kind_feed';
                        </script>";
                    }else{
                        echo "<script>
                            var q_kind_feed='';
                        </script>";
                    } ?>
                    <!--yang ini-->
                    <select name="q_kind_feed" id="q_kind_feed" class="select2">
                            <option value="Jawaban Benar Setelah Soal">Jawaban Benar Setelah Soal</option>
                            <option value="Kunci Jawaban Collapse">Kunci Jawaban Collapse</option>
                            <option value="Kunci Jawaban Collapse di Akhir">Kunci Jawaban Collapse di Akhir + Ada Waktu</option>
                            <option value="Tidak Ada Kunci Jawaban">Tidak Ada Kunci Jawaban + Ada Waktu</option>
                    </select>
                </div>
            </div><!--.row-->
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="longtext">Cover</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                            <a href="#" data-toggle="modal" data-target="#upload3Modal" class="btn btn-primary col-md-12">Klik untuk menambahkan gambar</a>
                                
                                <div class="modal fade" id="upload3Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                                                    <i class="font-icon-close-2"></i>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Upload File</h4>
                                            </div>
                                            <div class="modal-upload menu-big-icons">
                                                <div class="modal-upload-cont">
                                                    <div class="modal-upload-cont-in">
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" style="padding:10px;" id="tab-upload-3-1">
                                                                <input type="file" id="upload_avatar"/>
                                                            </div><!--.tab-pane-->
                                                            <div role="tabpanel" class="tab-pane" style="padding:10px;" id="tab-upload-3-2">
                                                                <div style="float:left;">
                                                                    <div id="jcrop">Click button above to choose avatar.</div>
                                                                </div>
                                                                <div style="clear:both;"></div>
                                                                <b>Result :</b><br>
                                                                <div style="float:left; margin-bottom:10px;">
                                                                    <canvas id="canvas"></canvas>
                                                                </div>
                                                            </div><!--.tab-pane-->
                                                        </div><!--.tab-content-->
                                                    </div><!--.modal-upload-cont-in-->
                                                </div><!--.modal-upload-cont-->
                                                <div class="modal-upload-side">
                                                    <ul class="upload-list" role="tablist">
                                                        <li class="nav-item">
                                                            <a href="#tab-upload-3-1" role="tab" data-toggle="tab" class="active">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="#tab-upload-3-2" role="tab" data-toggle="tab">
                                                                <i class="glyphicon glyphicon-picture"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--.modal-->
                
                <input type="hidden" class="form-control" name="q_cover" id="avatar" placeholder="Q Cover" value="<?php echo $q_cover; ?>">
                
            </p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="varchar">Nama Paket Soal</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control" name="q_title" id="q_title" placeholder="Misal: Latihan, UTS atau UAS" value="<?php echo $q_title; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="varchar">Waktu (dalam menit)</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control time-mask-input" name="q_time" id="q_time" placeholder="Q Time" value="<?php echo $q_time; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="date">Tanggal Rilis Soal</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control date-mask-input inputDate" name="q_post_publish_date" id="q_post_publish_date" placeholder="Q Post Publish Date" value="<?php echo $q_post_publish_date; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="date">Masa Kadaluarsa</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control date-mask-input inputDate" name="q_post_expired" id="q_post_expired" placeholder="Q Post Expired" value="<?php echo $q_post_expired; ?>"></p>
        </div>
    </div>
	    <input type="hidden" name="q_id" value="<?php echo $q_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('quiz_develop') ?>" class="btn btn-default">Batal</a>
	</form>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>