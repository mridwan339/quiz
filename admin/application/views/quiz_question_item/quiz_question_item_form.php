<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
    
        <h2 style="margin-top:0px"> <?php if($button=="Update"){ echo "Edit"; }else{ echo "Tambah"; } ?> Quiz Item</h2>
		<?php echo form_error('q_question_id'); ?>
        <?php echo form_error('q_no_question'); ?>
        <?php echo form_error('q_question_image'); ?>
        <?php echo form_error('q_question'); ?>
        <?php echo form_error('q_multipleChoices'); ?>
        <?php echo form_error('q_answerkey'); ?>
        <?php echo form_error('q_description'); ?>
        <section class="box-typical card-block">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="int">Nama Quiz</label>
        <div class="col-sm-10">
            <?php /* ?>
            <p class="form-control-static"><input class="form-control" name="q_question_id" id="q_question_id" placeholder="Q Question Id" value="<?php echo $q_question_id; ?>"></p>
            <?php */ ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_question_id !=="" ){
                        echo "<script>
                            var q_question_id='$q_question_id';
                        </script>";
                    }else{
                        echo "<script>
                            var q_question_id='';
                        </script>";
                    } ?>
                    <!--yang ini-->
                    <select name="q_question_id" id="q_question_id" class="select2">
                        <?php foreach ($quiz as $r):?>
                            <option value="<?php echo $r->q_id; ?>"><?php echo $r->q_title; ?></option>
                        <?php endforeach;?>
                            
                    </select>
                </div>
            </div><!--.row-->
        </div>
    </div>
    <?php
    if($button=="Buat"){?>
    <script type="text/javascript">
    $(document).ready(function(){
        q_question_id='<?php echo $idQuiz; ?>';
        $("#q_question_id").on("change",function(){
            window.location="<?php echo base_url(); ?>index.php/quiz_question_item/create/"+$(this).val();
        });
    });
    </script>
    <?php 

    if($idQuiz !=="" ){
        ?>





<div class="form-group row">
        <label class="col-sm-2 form-control-label" for="int">Soal Nomor</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control" name="q_no_question" id="q_no_question" placeholder="Nomor Soal" value="<?php echo $q_no_question; ?>"></p>
        </div>
    </div>
        <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="longtext">Soal Gambar</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="#" data-toggle="modal" data-target="#upload3Modal" class="btn btn-primary col-md-12">Klik untuk memsukan gambar</a>
                                
                                <div class="modal fade" id="upload3Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                                                    <i class="font-icon-close-2"></i>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Upload File</h4>
                                            </div>
                                            <div class="modal-upload menu-big-icons">
                                                <div class="modal-upload-cont">
                                                    <div class="modal-upload-cont-in">
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" style="padding:10px;" id="tab-upload-3-1">
                                                                <input type="file" id="upload_avatar"/>
                                                            </div><!--.tab-pane-->
                                                            <div role="tabpanel" class="tab-pane" style="padding:10px;" id="tab-upload-3-2">
                                                                <div style="float:left;">
                                                                    <div id="jcrop">Click button above to choose avatar.</div>
                                                                </div>
                                                                <div style="clear:both;"></div>
                                                                <b>Result :</b><br>
                                                                <div style="float:left; margin-bottom:10px;">
                                                                    <canvas id="canvas"></canvas>
                                                                </div>
                                                            </div><!--.tab-pane-->
                                                        </div><!--.tab-content-->
                                                    </div><!--.modal-upload-cont-in-->
                                                </div><!--.modal-upload-cont-->
                                                <div class="modal-upload-side">
                                                    <ul class="upload-list" role="tablist">
                                                        <li class="nav-item">
                                                            <a href="#tab-upload-3-1" role="tab" data-toggle="tab" class="active">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="#tab-upload-3-2" role="tab" data-toggle="tab">
                                                                <i class="glyphicon glyphicon-picture"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--.modal-->
                <input type="hidden" class="form-control" name="q_question_image" id="avatar" placeholder="Q Question Image" value="<?php echo $q_question_image; ?>"></p>
        </div>
    </div>
        <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_question">Pertanyaan</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" name="q_question" id="q_question" placeholder="Pertanyaan"><?php echo $q_question; ?></textarea></p>
        </div>
    </div>
    
    <?php
    $exp=explode(";",$q_multipleChoices);
    ?>

    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban A</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzA" placeholder="Jawaban A"><?php if(isset($exp[0])){ echo $exp[0]; }else{} ?></textarea></p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban B</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzB" placeholder="Jawaban B"><?php if(isset($exp[1])){ echo $exp[1]; }else{} ?></textarea></p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban C</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzC" placeholder="Jawaban C"><?php if(isset($exp[2])){ echo $exp[2]; }else{} ?></textarea></p>
        </div>
    </div>
    <?php if($Cquiz_typeAlpha=="" || $Cquiz_typeAlpha=="abcd" || $Cquiz_typeAlpha=="abcde"){ ?>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban D</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzD" placeholder="Jawaban D"><?php if(isset($exp[3])){ echo $exp[3]; }else{} ?></textarea></p>
        </div>
    </div>
    <?php }
    if($Cquiz_typeAlpha=="" || $Cquiz_typeAlpha=="abcde"){ ?>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban E</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzE" placeholder="Jawaban E"><?php if(isset($exp[4])){ echo $exp[4]; }else{} ?></textarea></p>
        </div>
    </div>
    <?php } ?>
        <script type="text/javascript">
            var vqza;
            var vqzb;
            var vqzc;
            var vqzd;
            var vqze;
            setInterval(function(){
                
                if($("#qzA").val() !==undefined ){
                    vqza=$("#qzA").val();
                }else{
                    vqza="";
                }
                if($("#qzB").val() !==undefined ){
                    vqzb=$("#qzB").val();
                    var sparateA=";";
                }else{
                    vqzb="";
                    var sparateA="";
                }
                if($("#qzC").val() !==undefined ){
                    vqzc=$("#qzC").val();
                    var sparateB=";";
                }else{
                    vqzc="";
                    var sparateB="";
                }
                if($("#qzD").val() !==undefined ){
                    vqzd=$("#qzD").val();
                    var sparateC=";";
                }else{
                    vqzd="";
                    var sparateC="";
                }
                if($("#qzE").val() !==undefined ){
                    vqze=$("#qzE").val();
                    var sparateD=";";
                }else{
                    vqze="";
                    var sparateD="";
                }

                $("#q_multipleChoices").val(vqza+sparateA+vqzb+sparateB+vqzc+sparateC+vqzd+sparateD+vqze);
            },1000);

        </script>

        <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Multiple Choices</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" readonly="readonly" name="q_multipleChoices" id="q_multipleChoices" placeholder="Q MultipleChoices"><?php echo $q_multipleChoices; ?></textarea></p>
        </div>
    </div>
        <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_answerkey">Kunci Jawaban</label>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_answerkey !=="" ){
                        echo "<script>
                            var select_answer='$q_answerkey';
                        </script>";
                    }else{
                        echo "<script>
                            var select_answer='';
                        </script>";
                    } ?>
                    <script type="text/javascript">
                    $(document).ready(function(){
                        var ass="";

                $("#select_answer").append("<option value=''>Pilih Kunci Jawaban</option>");
                        if($("#qzA").val() !==undefined ){
                    vqza=$("#qzA").val();
                    $("#select_answer").append("<option value='"+vqza+"'>A</option>");
                }else{
                    vqza="";
                }
                if($("#qzB").val() !==undefined ){
                    vqzb=$("#qzB").val();
                    var sparateA=";";
                    $("#select_answer").append("<option value='"+vqzb+"'>B</option>");
                }else{
                    vqzb="";
                    var sparateA="";
                }
                if($("#qzC").val() !==undefined ){
                    vqzc=$("#qzC").val();
                    var sparateB=";";
                    $("#select_answer").append("<option value='"+vqzc+"'>C</option>");
                }else{
                    vqzc="";
                    var sparateB="";
                }
                if($("#qzD").val() !==undefined ){
                    vqzd=$("#qzD").val();
                    var sparateC=";";
                    $("#select_answer").append("<option value='"+vqzd+"'>D</option>");
                }else{
                    vqzd="";
                    var sparateC="";
                }
                if($("#qzE").val() !==undefined ){
                    vqze=$("#qzE").val();
                    var sparateD=";";
                    $("#select_answer").append("<option value='"+vqze+"'>E</option>");
                }else{
                    vqze="";
                    var sparateD="";
                }


                $("#select_answer").on('select2:opening', function (e) {
                    

                
                $("#select_answer").html("");

                $("#select_answer").append("<option value=''>Pilih Kunci Jawaban</option>");

                if($("#qzA").val() !==undefined ){
                    vqza=$("#qzA").val();
                    $("#select_answer").append("<option value='"+vqza+"'>A</option>");
                }else{
                    vqza="";
                }
                if($("#qzB").val() !==undefined ){
                    vqzb=$("#qzB").val();
                    var sparateA=";";
                    $("#select_answer").append("<option value='"+vqzb+"'>B</option>");
                }else{
                    vqzb="";
                    var sparateA="";
                }
                if($("#qzC").val() !==undefined ){
                    vqzc=$("#qzC").val();
                    var sparateB=";";
                    $("#select_answer").append("<option value='"+vqzc+"'>C</option>");
                }else{
                    vqzc="";
                    var sparateB="";
                }
                if($("#qzD").val() !==undefined ){
                    vqzd=$("#qzD").val();
                    var sparateC=";";
                    $("#select_answer").append("<option value='"+vqzd+"'>D</option>");
                }else{
                    vqzd="";
                    var sparateC="";
                }
                if($("#qzE").val() !==undefined ){
                    vqze=$("#qzE").val();
                    var sparateD=";";
                    $("#select_answer").append("<option value='"+vqze+"'>E</option>");
                }else{
                    vqze="";
                    var sparateD="";
                }
                });
                $("#select_answer").on("change",function(){
                
                    ass=$(this).val();
                    
                
                $(this).val(ass);
                    $("#q_answerkey").val($(this).val());
                });


                    });</script>
                    <!--yang ini-->
                    <select id="select_answer" class="select2">
                            
                            
                    </select>
                </div>
            </div><!--.row-->
            <p class="form-control-static"><textarea class="form-control" rows="3" name="q_answerkey" readonly="readonly" id="q_answerkey" placeholder="Q Answerkey"><?php echo $q_answerkey; ?></textarea></p>
        </div>
    </div>
        <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_description">Deskripsi</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" name="q_description" id="q_description" placeholder="Deskripsi"><?php echo $q_description; ?></textarea></p>
        </div>
    </div>










        <?php
    }

    }else{
    ?>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="int">Soal Nomor</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control" name="q_no_question" id="q_no_question" placeholder="Nomor Soal ke" value="<?php echo $q_no_question; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="longtext">Soal Gambar</label>
        <div class="col-sm-10">
            <p class="form-control-static">
                <a href="#" data-toggle="modal" data-target="#upload3Modal" class="btn btn-primary col-md-12">Klik untuk memasukan gambar</a>
                                
                                <div class="modal fade" id="upload3Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                                                    <i class="font-icon-close-2"></i>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">Upload File</h4>
                                            </div>
                                            <div class="modal-upload menu-big-icons">
                                                <div class="modal-upload-cont">
                                                    <div class="modal-upload-cont-in">
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active" style="padding:10px;" id="tab-upload-3-1">
                                                                <input type="file" id="upload_avatar"/>
                                                            </div><!--.tab-pane-->
                                                            <div role="tabpanel" class="tab-pane" style="padding:10px;" id="tab-upload-3-2">
                                                                <div style="float:left;">
                                                                    <div id="jcrop">Click button above to choose avatar.</div>
                                                                </div>
                                                                <div style="clear:both;"></div>
                                                                <b>Result :</b><br>
                                                                <div style="float:left; margin-bottom:10px;">
                                                                    <canvas id="canvas"></canvas>
                                                                </div>
                                                            </div><!--.tab-pane-->
                                                        </div><!--.tab-content-->
                                                    </div><!--.modal-upload-cont-in-->
                                                </div><!--.modal-upload-cont-->
                                                <div class="modal-upload-side">
                                                    <ul class="upload-list" role="tablist">
                                                        <li class="nav-item">
                                                            <a href="#tab-upload-3-1" role="tab" data-toggle="tab" class="active">
                                                                <i class="glyphicon glyphicon-upload"></i>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item">
                                                            <a href="#tab-upload-3-2" role="tab" data-toggle="tab">
                                                                <i class="glyphicon glyphicon-picture"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--.modal-->
                <input type="hidden" class="form-control" name="q_question_image" id="avatar" placeholder="Q Question Image" value="<?php echo $q_question_image; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_question">Pertanyaan</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" name="q_question" id="q_question" placeholder="Pertanyaan"><?php echo $q_question; ?></textarea></p>
        </div>
    </div>
    
    <?php
    $exp=explode(";",$q_multipleChoices);
    ?>

    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban A</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzA" placeholder="Jawaban A"><?php if(isset($exp[0])){ echo $exp[0]; }else{} ?></textarea></p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban B</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzB" placeholder="Jawaban B"><?php if(isset($exp[1])){ echo $exp[1]; }else{} ?></textarea></p>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban C</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzC" placeholder="Jawaban C"><?php if(isset($exp[2])){ echo $exp[2]; }else{} ?></textarea></p>
        </div>
    </div>
    <?php if($Cquiz_typeAlpha=="" || $Cquiz_typeAlpha=="abcd" || $Cquiz_typeAlpha=="abcde"){ ?>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban D</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzD" placeholder="Jawaban D"><?php if(isset($exp[3])){ echo $exp[3]; }else{} ?></textarea></p>
        </div>
    </div>
    <?php }
    if($Cquiz_typeAlpha=="" || $Cquiz_typeAlpha=="abcde"){ ?>
    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Jawaban E</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" id="qzE" placeholder="Jawaban E"><?php if(isset($exp[4])){ echo $exp[4]; }else{} ?></textarea></p>
        </div>
    </div>
    <?php } ?>
        <script type="text/javascript">
            var vqza;
            var vqzb;
            var vqzc;
            var vqzd;
            var vqze;
            setInterval(function(){
                
                if($("#qzA").val() !==undefined ){
                    vqza=$("#qzA").val();
                }else{
                    vqza="";
                }
                if($("#qzB").val() !==undefined ){
                    vqzb=$("#qzB").val();
                    var sparateA=";";
                }else{
                    vqzb="";
                    var sparateA="";
                }
                if($("#qzC").val() !==undefined ){
                    vqzc=$("#qzC").val();
                    var sparateB=";";
                }else{
                    vqzc="";
                    var sparateB="";
                }
                if($("#qzD").val() !==undefined ){
                    vqzd=$("#qzD").val();
                    var sparateC=";";
                }else{
                    vqzd="";
                    var sparateC="";
                }
                if($("#qzE").val() !==undefined ){
                    vqze=$("#qzE").val();
                    var sparateD=";";
                }else{
                    vqze="";
                    var sparateD="";
                }

                $("#q_multipleChoices").val(vqza+sparateA+vqzb+sparateB+vqzc+sparateC+vqzd+sparateD+vqze);
            },1000);

        </script>

	    <div class="form-group row" hidden>
        <label class="col-sm-2 form-control-label" for="q_multipleChoices">Multiple Choices</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" readonly="readonly" name="q_multipleChoices" id="q_multipleChoices" placeholder="Q MultipleChoices"><?php echo $q_multipleChoices; ?></textarea></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_answerkey">Kunci Jawaban</label>
        <div class="col-sm-10">
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_answerkey !=="" ){
                        echo "<script>
                            var select_answer='$q_answerkey';
                        </script>";
                    }else{
                        echo "<script>
                            var select_answer='';
                        </script>";
                    } ?>
                    <script type="text/javascript">
                    $(document).ready(function(){
                        var ass="";

                $("#select_answer").append("<option value=''>Pilih Kunci Jawaban</option>");
                        if($("#qzA").val() !==undefined ){
                    vqza=$("#qzA").val();
                    $("#select_answer").append("<option value='"+vqza+"'>A</option>");
                }else{
                    vqza="";
                }
                if($("#qzB").val() !==undefined ){
                    vqzb=$("#qzB").val();
                    var sparateA=";";
                    $("#select_answer").append("<option value='"+vqzb+"'>B</option>");
                }else{
                    vqzb="";
                    var sparateA="";
                }
                if($("#qzC").val() !==undefined ){
                    vqzc=$("#qzC").val();
                    var sparateB=";";
                    $("#select_answer").append("<option value='"+vqzc+"'>C</option>");
                }else{
                    vqzc="";
                    var sparateB="";
                }
                if($("#qzD").val() !==undefined ){
                    vqzd=$("#qzD").val();
                    var sparateC=";";
                    $("#select_answer").append("<option value='"+vqzd+"'>D</option>");
                }else{
                    vqzd="";
                    var sparateC="";
                }
                if($("#qzE").val() !==undefined ){
                    vqze=$("#qzE").val();
                    var sparateD=";";
                    $("#select_answer").append("<option value='"+vqze+"'>E</option>");
                }else{
                    vqze="";
                    var sparateD="";
                }


                $("#select_answer").on('select2:opening', function (e) {
                    

                
                $("#select_answer").html("");

                $("#select_answer").append("<option value=''>Pilih Kunci Jawaban</option>");

                if($("#qzA").val() !==undefined ){
                    vqza=$("#qzA").val();
                    $("#select_answer").append("<option value='"+vqza+"'>A</option>");
                }else{
                    vqza="";
                }
                if($("#qzB").val() !==undefined ){
                    vqzb=$("#qzB").val();
                    var sparateA=";";
                    $("#select_answer").append("<option value='"+vqzb+"'>B</option>");
                }else{
                    vqzb="";
                    var sparateA="";
                }
                if($("#qzC").val() !==undefined ){
                    vqzc=$("#qzC").val();
                    var sparateB=";";
                    $("#select_answer").append("<option value='"+vqzc+"'>C</option>");
                }else{
                    vqzc="";
                    var sparateB="";
                }
                if($("#qzD").val() !==undefined ){
                    vqzd=$("#qzD").val();
                    var sparateC=";";
                    $("#select_answer").append("<option value='"+vqzd+"'>D</option>");
                }else{
                    vqzd="";
                    var sparateC="";
                }
                if($("#qzE").val() !==undefined ){
                    vqze=$("#qzE").val();
                    var sparateD=";";
                    $("#select_answer").append("<option value='"+vqze+"'>E</option>");
                }else{
                    vqze="";
                    var sparateD="";
                }
                });
                $("#select_answer").on("change",function(){
                
                    ass=$(this).val();
                    
                
                $(this).val(ass);
                    $("#q_answerkey").val($(this).val());
                });


                    });</script>
                    <!--yang ini-->
                    <select id="select_answer" class="select2">
                            
                            
                    </select>
                </div>
            </div><!--.row-->
            <p class="form-control-static"><textarea class="form-control" rows="3" name="q_answerkey" readonly="readonly" id="q_answerkey" placeholder="Kunci Jawaban"><?php echo $q_answerkey; ?></textarea></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="q_description">Deskripsi</label>
        <div class="col-sm-10">
            <p class="form-control-static"><textarea class="form-control" rows="3" name="q_description" id="q_description" placeholder="Deskripsi"><?php echo $q_description; ?></textarea></p>
        </div>
    </div>
    <?php } ?>
	    <input type="hidden" name="q_itemid" value="<?php echo $q_itemid; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('quiz_question_item') ?>" class="btn btn-danger">Batal</a>
	</form>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>