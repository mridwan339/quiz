<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
        <h2 style="margin-top:0px">Detail Quiz Item</h2>
        <section class="box-typical card-block">
        <table class="table">
	    <tr><td style='border-top: none !important;'>Nama Quiz</td><td style='border-top: none !important;'><?php echo $q_title; ?></td></tr>
	    <tr><td >Soal Nomor</td><td ><?php echo $q_no_question; ?></td></tr>
	    <tr><td >Soal Gambar</td><td ><?php if($q_question_image==""){ echo "Tidak ada gambar tersedia"; }else{ ?><img src="<?php echo $q_question_image; ?>" width="100%"><?php } ?></td></tr>
	    <tr><td >Pertanyaan</td><td ><?php echo $q_question; ?></td></tr>
	    <tr><td >Pilihan Ganda</td><td ><?php echo $q_multipleChoices; ?></td></tr>
	    <tr><td>Kunci Jawaban</td><td ><?php echo $q_answerkey; ?></td></tr>
	    <tr><td>Deskripsi</td><td ><?php echo $q_description; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('quiz_question_item') ?>" class="btn btn-default">Kembali ke List</a></td></tr>
	</table>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>