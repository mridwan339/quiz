<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3><?php echo lang('edit_group_heading');?></h3>
							<h6><?php echo lang('edit_group_subheading');?></h6>
							<div id="infoMessage"><?php echo $message;?></div>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<div class="row">
					<?php echo form_open(current_url());?>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_group_name_label', 'group_name');?></label>
								<?php echo form_input($group_name);?>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_group_desc_label', 'description');?></label>
								<?php echo form_input($group_description);?>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset class="form-group">
								<?php echo form_submit('submit', lang('edit_group_submit_btn'),"class='btn col-md-6 col-xs-12'");?>
								<!--<button type="submit" class="btn col-md-6 col-xs-12">Save</button>-->
							</fieldset>
						</div>
					<?php echo form_close();?>
					</div><!--.row-->
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
<?php
Template::extra();
Template::footer();
?>