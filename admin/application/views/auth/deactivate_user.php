<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3><?php echo lang('deactivate_heading');?></h3>
							<h6><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></h6>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<div class="row">
					<?php echo form_open("auth/deactivate/".$user->id);?>
						<div class="col-md-3">
							<fieldset class="form-group">
								<div class="form-group">
									<div class="btn-group" data-toggle="buttons">
										<label class="btn active">
											<input type="radio" name="confirm" value="yes" autocomplete="off" checked=""> <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
										</label>
										<label class="btn">
											<input type="radio" name="confirm" value="no" /> <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
										</label>
									</div>
								</div>
							</fieldset>
						</div>
						<?php echo form_hidden($csrf); ?>
						<?php echo form_hidden(array('id'=>$user->id)); ?>
						<div class="col-md-9">
							<fieldset class="form-group">
								<?php echo form_submit('submit', lang('deactivate_submit_btn'),"class='btn col-md-6 col-xs-12'");?>
								<!--<button type="submit" class="btn col-md-6 col-xs-12">Save</button>-->
							</fieldset>
						</div>
					<?php echo form_close();?>
					</div><!--.row-->
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
<?php
Template::extra();
Template::footer();
?>