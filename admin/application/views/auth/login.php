<?php
Template::auth_header();
Template::auth_sidebar();
?>
<div class="page-center">
	<div class="page-center-in">
		<div class="container-fluid">
			<?php echo form_open("auth/login",array("class"=>"sign-box"));?>
				<div class="sign-avatar">
					<img src="<?php echo base_url(); ?>templates/StartUI/build/img/person-flat.png" alt="">
				</div>
				<header class="sign-title"><?php echo lang('login_heading');?></header>
				<p><?php echo lang('login_subheading');?></p>
				<p>
					
					<?php echo $message;?>
					
				</p>
				<div class="form-group">
					<?php echo form_input($identity);?>
				</div>
				<div class="form-group">
					<?php echo form_input($password);?>
				</div>
				<div class="form-group">
					<div class="checkbox float-left">
						<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); echo lang('login_remember_label', 'remember');?>
					</div>
					<div class="float-right reset">
						<a href="forgot_password"><?php echo lang('login_forgot_password');?></a>
					</div>
				</div>
				<?php echo form_submit($submit);?>
				<p class="sign-note">Kembali ke Beranda <i class="fa fa-home"></i>? <a href="../../../">Klik Disini</a></p>
					<button type="button" class="close">
					<span aria-hidden="true">&times;</span>
				</button>
			<?php echo form_close();?>
		</div>
	</div>
</div>

<?php
Template::auth_extra();
Template::auth_footer();
?>