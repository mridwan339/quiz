<?php
Template::auth_header();
Template::auth_sidebar();
?>
<div class="page-center">
	<div class="page-center-in">
		<div class="container-fluid">
			<?php echo form_open('auth/reset_password/' . $code,array("class"=>"sign-box"));?>
				<div class="sign-avatar">
					<img src="<?php echo base_url(); ?>templates/StartUI/build/img/avatar-sign.png" alt="">
				</div>
				<header class="sign-title"><?php echo lang('reset_password_heading');?></header>
				<p>
					
					<?php echo $message;?>
					
				</p>
				<div class="form-group">
					<?php echo form_input($new_password);?>
				</div>
				<div class="form-group">
					<?php echo form_input($new_password_confirm);?>
				</div>
				<?php echo form_input($user_id);?>
				<?php echo form_hidden($csrf); ?>
				
				<?php echo form_submit($submit);?>
				<!--<p class="sign-note">New to our website? <a href="sign-up.html">Sign up</a></p>
					<button type="button" class="close">
					<span aria-hidden="true">&times;</span>
				</button>-->
			<?php echo form_close();?>
		</div>
	</div>
</div>

<?php
Template::auth_extra();
Template::auth_footer();
?>