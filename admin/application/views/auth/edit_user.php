<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3><?php echo lang('edit_user_heading');?></h3>
							<h6><?php echo lang('edit_user_subheading');?></h6>
							<div id="infoMessage"><?php echo $message;?></div>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<div class="row">
					<?php echo form_open(uri_string());?>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_user_fname_label', 'first_name');?></label>
								<?php echo form_input($first_name);?>
								<!--<input name="signin_v1[username]"
									   type="text"
									   class="form-control"
									   data-validation="[NOTEMPTY]">-->
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_user_lname_label', 'last_name');?></label>
								<?php echo form_input($last_name);?>
								<!--<input name="signin_v1[password]"
									   type="password"
									   class="form-control"
									   data-validation="[NOTEMPTY]">-->
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_user_avatar_label', 'avatar');?></label>
								<a href="#" data-toggle="modal" data-target="#upload3Modal" class="btn btn-primary col-md-12">Klik untuk merubah avatar</a>
								
								<div class="modal fade" id="upload3Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
													<i class="font-icon-close-2"></i>
												</button>
												<h4 class="modal-title" id="myModalLabel">Upload File</h4>
											</div>
											<div class="modal-upload menu-big-icons">
												<div class="modal-upload-cont">
													<div class="modal-upload-cont-in">
														<div class="tab-content">
															<div role="tabpanel" class="tab-pane active" style="padding:10px;" id="tab-upload-3-1">
																<input type="file" id="upload_avatar"/>
															</div><!--.tab-pane-->
															<div role="tabpanel" class="tab-pane" style="padding:10px;" id="tab-upload-3-2">
																<div style="float:left;">
																	<div id="jcrop">Click button above to choose avatar.</div>
																</div>
																<div style="clear:both;"></div>
																<b>Result :</b><br>
																<div style="float:left; margin-bottom:10px;">
																	<canvas id="canvas"></canvas>
																</div>
															</div><!--.tab-pane-->
														</div><!--.tab-content-->
													</div><!--.modal-upload-cont-in-->
												</div><!--.modal-upload-cont-->
												<div class="modal-upload-side">
													<ul class="upload-list" role="tablist">
														<li class="nav-item">
															<a href="#tab-upload-3-1" role="tab" data-toggle="tab" class="active">
																<i class="glyphicon glyphicon-upload"></i>
															</a>
														</li>
														<li class="nav-item">
															<a href="#tab-upload-3-2" role="tab" data-toggle="tab">
																<i class="glyphicon glyphicon-picture"></i>
															</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div><!--.modal-->
								<?php echo form_input($avatar);?>
								<!--<input name="signin_v2[username]"
									   type="text"
									   class="form-control"
									   data-validation="[NOTEMPTY]">-->
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_user_phone_label', 'avatar');?></label>
								<?php echo form_input($phone);?>
								<!--<input name="signin_v2[username]"
									   type="text"
									   class="form-control"
									   data-validation="[NOTEMPTY]">-->
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_user_password_label', 'avatar');?></label>
								<?php echo form_input($password);?>
								<!--<input name="signin_v2[username]"
									   type="text"
									   class="form-control"
									   data-validation="[NOTEMPTY]">-->
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_user_password_confirm_label', 'email');?></label>
								<?php echo form_input($password_confirm);?>
								<!--<input name="signin_v2[password]"
									   type="password"
									   class="form-control"
									   data-validation="[NOTEMPTY]">-->
							</fieldset>
						</div>
						<?php if ($this->ion_auth->is_admin()): ?>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('edit_user_groups_heading');?></label>
								<?php foreach ($groups as $group):?>
								  <?php
									  $gID=$group['id'];
									  $checked = null;
									  $item = null;
									  foreach($currentGroups as $grp) {
										  if ($gID == $grp->id) {
											  $checked= ' checked="checked"';
										  break;
										  }
									  }
								  ?>
									<div class="radio">
										<input type="radio" name="groups[]" id="<?php echo $group['id'];?>" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
										<label for="<?php echo $group['id'];?>"><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></label>
									</div>
								  
								<?php endforeach?>
							</fieldset>
						</div>
						<?php endif ?>
						
								
							
						<?php echo form_hidden('id', $user->id);?>
						<?php echo form_hidden($csrf); ?>
						<div class="col-md-6">
							<fieldset class="form-group">
								<?php echo form_submit('submit', lang('edit_user_submit_btn'),"class='btn col-md-6 col-xs-12'");?>
								<!--<button type="submit" class="btn col-md-6 col-xs-12">Save</button>-->
							</fieldset>
						</div>
					<?php echo form_close();?>
					</div><!--.row-->
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
<?php
Template::extra();
Template::footer();
?>