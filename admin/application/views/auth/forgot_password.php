<?php
Template::auth_header();
Template::auth_sidebar();
?>
<div class="page-center">
	<div class="page-center-in">
		<div class="container-fluid">
			<?php echo form_open("auth/forgot_password",array("class"=>"sign-box"));?>
				<div class="sign-avatar">
					<img src="<?php echo base_url(); ?>templates/StartUI/build/img/person-flat.png" alt="">
				</div>
				<header class="sign-title"><?php echo lang('forgot_password_heading');?></header>
				<p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
				<p>
					
					<?php echo $message;?>
					
				</p>
				<div class="form-group">
					<?php echo (($type=='email') ? sprintf(lang(''), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?>
					<br>
					<?php echo form_input($identity);?>
				</div>
				<?php echo form_submit($submit);?>
				<p class="sign-note">Kembali ke Login </i>? <a href="login/">Klik Disini</a></p>
				<!--<p class="sign-note">New to our website? <a href="sign-up.html">Sign up</a></p>
					<button type="button" class="close">
					<span aria-hidden="true">&times;</span>
				</button>-->
			<?php echo form_close();?>
		</div>
	</div>
</div>


<?php
Template::auth_extra();
Template::auth_footer();
?>