<?php
Template::header();
Template::sidebar();
?>
			

<div class="page-content">
	<div class="container-fluid">
		
		<!--<?php //echo lang('index_email_th');?>-->
		<div class="row">
		<?php foreach ($groups as $group):?>
			<div class="col-xs-6" style="cursor:pointer;" onclick="window.location='<?php echo "auth/edit_group/".$group->id; ?>'">
				<section class="widget widget-simple-sm">
					<div class="widget-simple-sm-icon">
						<i class="fa fa-group color-green"></i>
					</div>
					<div class="widget-simple-sm-bottom"><?php echo strtoupper(htmlspecialchars($group->name,ENT_QUOTES,'UTF-8')); ?></div>
				</section><!--.widget-simple-sm-->
			</div>
		<?php endforeach?>
			
			
		</div>
		<div id="users">
		<input class="search form-control col-md-9" placeholder="Cari" />
		<div style="clear:both; margin-bottom:10px;"></div>
  		<button class="sort" data-sort="fname">
			Urut Nama Depan
		</button>
		<button class="sort" data-sort="lname">
			Urut Nama Belakang
		</button>
		<button class="sort" data-sort="acname">
			Urut Status
		</button>
		<hr>
		<section class="box-typical">
			<header class="box-typical-header">
				<div class="tbl-row">
					<div class="tbl-cell tbl-cell-title">
						<h3><?php if($message !==NULL){ ?><div id="infoMessage"><?php echo $message;?></div><?php } else { echo lang('index_subheading'); } ?></h3>
					</div>
					<div class="tbl-cell tbl-cell-action-bordered">
						<button type="button" onclick="window.location='auth/create_user'" class="action-btn"><i class="fa fa-plus-circle"></i></button>
					</div>
					<div class="tbl-cell tbl-cell-action-bordered">
						<b><?php echo lang('index_heading');?></b>
					</div>
					
				</div>
			</header>
			<div class="box-typical-body">
				<div class="table-responsive">
					<table class="table table-hover">
						<thead>
							<tr>
								<th><?php echo lang('index_fname_th');?></th>
								<th><?php echo lang('index_lname_th');?></th>
								<th><?php echo lang('index_email_th');?></th>
								<th><?php echo lang('index_status_th');?></th>
								<th><?php echo lang('index_action_th');?></th>
							</tr>
						</thead>
						<tbody class="list">
							<?php foreach ($users as $user):?>
								<tr>
									<td class="fname"><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
									<td class="lname"><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
									<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
									<td class="acname"><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
									<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div><!--.box-typical-body-->
		</section>
		<div class="row col-md-12">
			<ul class="pagination btn-group" role="group"></ul>
		</div>
		</div>
	</div><!--.container-fluid-->
</div><!--.page-content-->
<!--
<p><?php // echo anchor('create_user', lang('index_create_user_link'))?> | <?php //echo anchor('create_group', lang('index_create_group_link'))?></p>-->
<?php
Template::extra();
Template::footer();
?>