<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
		<div class="container-fluid">
			<header class="section-header">
				<div class="tbl">
					<div class="tbl-row">
						<div class="tbl-cell">
							<h3><?php echo lang('change_password_heading');?></h3>
							<div id="infoMessage"><?php echo $message;?></div>
						</div>
					</div>
				</div>
			</header>

			<section class="card">
				<div class="card-block">
					<div class="row">
					<?php echo form_open("auth/change_password");?>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('change_password_old_password_label', 'old_password');?></label>
								<?php echo form_input($old_password);?>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset class="form-group">
								<label class="form-label"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label>
								<?php echo form_input($new_password);?>
							</fieldset>
							<fieldset class="form-group">
								<label class="form-label"><?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?></label>
								<?php echo form_input($new_password_confirm);?>
							</fieldset>
						</div>
						<div class="col-md-6">
							<fieldset class="form-group">
								<?php echo form_submit('submit', lang('change_password_submit_btn'),"class='btn col-md-6 col-xs-12'");?>
								<!--<button type="submit" class="btn col-md-6 col-xs-12">Save</button>-->
							</fieldset>
						</div>
					<?php echo form_close();?>
					</div><!--.row-->
				</div>
			</section>
		</div><!--.container-fluid-->
	</div><!--.page-content-->
<?php
Template::extra();
Template::footer();
?>