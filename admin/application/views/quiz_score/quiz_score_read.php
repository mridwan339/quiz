<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
        <h2 style="margin-top:0px">Detail Skor Quiz</h2>
        <section class="box-typical card-block">
        <table class="table">
	    <tr><td style='border-top: none !important;'>Username</td><td style='border-top: none !important;'><?php echo $username; ?></td></tr>
	    <tr><td >Quiz</td><td ><?php echo $q_title; ?></td></tr>
	    <tr><td >Skor</td><td ><?php echo $q_score; ?></td></tr>
	    <tr><td >Tanggal</td><td ><?php echo $q_date; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('quiz_score') ?>" class="btn btn-default">Kembali ke List</a></td></tr>
	</table>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>