<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
        <h2 style="margin-top:0px;">Daftar Skor Quiz</h2>
        <section class="box-typical">
        <div style="margin-bottom: 10px">
            <header class="box-typical-header">

                <div style="display: table-caption;">
                <form action="<?php echo site_url('quiz_score/index'); ?>" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('quiz_score'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Cari</button>
                        </span>
                    </div>
                </form>
                <div style=" clear:both; "></div>
                </div>


                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title">
                        <h3><?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : 'Di bawah ini list dari Skor Quiz.'; ?></h3>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <button type="button" onclick="window.location='<?php echo site_url('quiz_score/create'); ?>'" class="action-btn"><i class="fa fa-plus-circle"></i></button>
                    </div>
                    <div class="tbl-cell tbl-cell-action-bordered">
                        <b><?php echo 'Skor Quiz'; ?></b>
                    </div>
                    
                </div>
            </header>
            <div class="box-typical-body">
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                <th>No</th>
		<th>Username</th>
		<th>Quiz</th>
		<th>Skor</th>
		<th>Tanggal</th>
		<th>Aksi</th>
            </tr>
                        </thead>
                        <tbody>
                            <?php
            foreach ($quiz_score_data as $quiz_score)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $quiz_score->username ?></td>
			<td><?php echo $quiz_score->q_title ?></td>
			<td><?php echo $quiz_score->q_score ?></td>
			<td><?php echo $quiz_score->q_date ?></td>
			
<td style="text-align:center" width="200px">
<div class="btn-group btn-group-sm" role="group" aria-label="Basic example">
    <a href="<?php echo site_url('quiz_score/read/'.$quiz_score->q_idscore); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-eye-open "></i></button></a>
    <a href="<?php echo site_url('quiz_score/update/'.$quiz_score->q_idscore); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-edit "></i></button></a>
    <a href="<?php echo site_url('quiz_score/delete/'.$quiz_score->q_idscore); ?>"><button type="button" class="btn btn-default-outline"><i class=" glyphicon glyphicon-trash "></i></button></a>
</div>

			</td>
		</tr>
                <?php
            }
            ?>
                        </tbody>
                    </table>
                </div>
            </div>




            
    </section>
    <div class="row">
            <div class="col-md-6">
                <a href="#" style=" margin-bottom:10px; " class="btn btn-primary">Total List Skor : <?php echo $total_rows ?></a>
                <a href="<?php echo base_url(); ?>index.php/Quiz_score/print_data" style=" margin-bottom:10px; " target="_blank" class="btn btn-primary">Print</a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>