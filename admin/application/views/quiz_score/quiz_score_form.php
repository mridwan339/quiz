<?php
Template::header();
Template::sidebar();
?>
<div class="page-content">
    <div class="container-fluid">
    
        <h2 style="margin-top:0px"> <?php if($button=="Update"){ echo "Edit"; }else{ echo "New"; } ?> Skor Quiz</h2>
		<?php echo form_error('q_id_user'); ?>
        <?php echo form_error('q_id_question'); ?>
        <?php echo form_error('q_score'); ?>
        <?php echo form_error('q_date'); ?>
        <section class="box-typical card-block">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="int">Username</label>
        <div class="col-sm-10">
            <?php /* ?>
            <p class="form-control-static"><input class="form-control" name="q_id_user" id="q_id_user" placeholder="Q Id User" value="<?php echo $q_id_user; ?>"></p>
            <?php */ ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_id_user !=="" ){
                        echo "<script>
                            var creatorID='$q_id_user';
                        </script>";
                    }else{
                        echo "<script>
                            var creatorID='';
                        </script>";
                    } ?>
                    <!--yang ini-->
                    <select name="q_id_user" id="creatorID" class="select2 select2-photo">
                        <?php foreach ($users as $user):?>
                            <option value="<?php echo $user->id; ?>" data-photo="<?php echo $user->avatar; ?>"><?php echo $user->first_name." ".$user->last_name; ?></option>
                        <?php endforeach;?>
                        <!--<option data-photo="../../../templates/StartUI/build/img/photo-64-1.jpg">Jenny Hess</option>
                        <option data-photo="../../../templates/StartUI/build/img/photo-64-2.jpg">Elliot Fu</option>
                        <option data-photo="../../../templates/StartUI/build/img/photo-64-3.jpg">Stevie Felicano</option>
                        <option data-photo="../../../templates/StartUI/build/img/photo-64-4.jpg">Christian</option>-->
                    </select>
                </div>
            </div><!--.row-->
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="int">Quiz</label>
        <div class="col-sm-10">
            <?php /* ?>
            <p class="form-control-static"><input class="form-control" name="q_id_question" id="q_id_question" placeholder="Q Id Question" value="<?php echo $q_id_question; ?>"></p>
            <?php */ ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if( $q_id_question !=="" ){
                        echo "<script>
                            var q_question_id='$q_id_question';
                        </script>";
                    }else{
                        echo "<script>
                            var q_question_id='';
                        </script>";
                    } ?>
                    <!--yang ini-->
                    <select name="q_id_question" id="q_question_id" class="select2">
                        <?php foreach ($quiz as $r):?>
                            <option value="<?php echo $r->q_id; ?>"><?php echo $r->q_title; ?></option>
                        <?php endforeach;?>
                            
                    </select>
                </div>
            </div><!--.row-->
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="varchar">Skor</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control" name="q_score" id="q_score" placeholder="Skor" value="<?php echo $q_score; ?>"></p>
        </div>
    </div>
	    <div class="form-group row">
        <label class="col-sm-2 form-control-label" for="date">Tanggal</label>
        <div class="col-sm-10">
            <p class="form-control-static"><input class="form-control" name="q_date" id="q_date" placeholder="Tanggal" value="<?php echo $q_date; ?>"></p>
        </div>
    </div>
	    <input type="hidden" name="q_idscore" value="<?php echo $q_idscore; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('quiz_score') ?>" class="btn btn-danger">Batal</a>
	</form>
        </section>
    </div>
</div>
<?php
Template::extra();
Template::footer();
?>