<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quiz_score_model extends CI_Model
{

    public $table = 'quiz_score';
    public $id = 'q_idscore';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->join( 'users us1', 'us1.id = quiz_score.q_id_user');
        $this->db->join( 'quiz_question qq', 'qq.q_id = quiz_score.q_id_question');
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        $this->db->join( 'users us1', 'us1.id = quiz_score.q_id_user');
        $this->db->join( 'quiz_question qq', 'qq.q_id = quiz_score.q_id_question');
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('q_idscore', $q);
    //$this->db->or_like('q_id_user', $q);
    //$this->db->or_like('q_id_question', $q);
    $this->db->or_like('q_score', $q);
    $this->db->or_like('q_date', $q);
    $this->db->or_like('us1.email', $q);
    $this->db->or_like('us1.username', $q);
    $this->db->join( 'users us1', 'us1.id = quiz_score.q_id_user');
    $this->db->or_like('qq.q_title', $q);
    $this->db->join( 'quiz_question qq', 'qq.q_id = quiz_score.q_id_question');
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->or_like('q_idscore', $q);
	//$this->db->or_like('q_id_user', $q);
	//$this->db->or_like('q_id_question', $q);
	$this->db->or_like('q_score', $q);
	$this->db->or_like('q_date', $q);
	$this->db->or_like('us1.email', $q);
	$this->db->or_like('us1.username', $q);
	$this->db->join( 'users us1', 'us1.id = quiz_score.q_id_user');
	$this->db->or_like('qq.q_title', $q);
	$this->db->join( 'quiz_question qq', 'qq.q_id = quiz_score.q_id_question');
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}
