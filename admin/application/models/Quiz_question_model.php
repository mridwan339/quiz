<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Quiz_question_model extends CI_Model
{

    public $table = 'quiz_question';
    public $id = 'q_id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->join( 'users us1', 'us1.id = quiz_question.q_creator_id');
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        // $this->db->where($this->ids, $ids);
        $this->db->join( 'users us1', 'us1.id = quiz_question.q_creator_id');
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        //$this->db->like('q_id', $q);
    //$this->db->or_like('q_creator_id', $q);
    $this->db->like('q_total', $q);
    $this->db->or_like('q_type_alpha', $q);
    $this->db->or_like('q_kind_feed', $q);
    $this->db->or_like('q_cover', $q);
    $this->db->or_like('q_title', $q);
    $this->db->or_like('q_time', $q);
    $this->db->or_like('q_post_publish_date', $q);
    $this->db->or_like('q_post_expired', $q);
    $this->db->or_like('us1.email', $q);
    $this->db->or_like('us1.username', $q);
    $this->db->join( 'users us1', 'us1.id = quiz_question.q_creator_id');  
    // 'users us1', 'us1.id = quiz_question.q_creator_id'
    $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        
    //$this->db->or_like('q_creator_id', $q);
    //$this->db->or_like('q_creator_id', $q);
    $this->db->like('q_total', $q);
    $this->db->or_like('q_type_alpha', $q);
    $this->db->or_like('q_kind_feed', $q);
    $this->db->or_like('q_cover', $q);
    $this->db->or_like('q_title', $q);
    $this->db->or_like('q_time', $q);
    $this->db->or_like('q_post_publish_date', $q);
    $this->db->or_like('q_post_expired', $q);
    $this->db->or_like('us1.email', $q);
    $this->db->or_like('us1.username', $q);
    $this->db->join( 'users us1', 'us1.id = quiz_question.q_creator_id');
    $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}